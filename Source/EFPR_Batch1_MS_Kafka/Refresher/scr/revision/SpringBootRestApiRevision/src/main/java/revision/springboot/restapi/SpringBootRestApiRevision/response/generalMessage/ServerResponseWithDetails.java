
/*
 * Represents the format in which a "response message" with the "processed information" is responded to the client.
 */

package revision.springboot.restapi.SpringBootRestApiRevision.response.generalMessage;

import revision.springboot.restapi.SpringBootRestApiRevision.entities.VehicleServiceRecord;

import java.util.List;

@SuppressWarnings("unused")
public record ServerResponseWithDetails(ServerResponse response, List<VehicleServiceRecord> serviceRecords) {
}
