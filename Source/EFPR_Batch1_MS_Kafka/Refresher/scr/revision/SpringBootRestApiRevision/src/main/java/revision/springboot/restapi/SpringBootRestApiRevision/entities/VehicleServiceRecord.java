
/*
 * This class is a template of a service record of a vehicle.
 */

package revision.springboot.restapi.SpringBootRestApiRevision.entities;

import jakarta.persistence.*;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Table(name = "service_records")
@Entity
@SuppressWarnings("unused")
public class VehicleServiceRecord {

    // Fields.
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "service_id")
    private UUID serviceID;

    @Column(nullable = false)
    private Date serviceDate;

    @Column(nullable = false)
    private String vehicleRegistrationNumber;

    // Default Constructor.
    public VehicleServiceRecord() {
        this.serviceDate = new Date();
    }

    // Parameterized Constructor.
    public VehicleServiceRecord(Date serviceDate, String vehicleRegistrationNumber) {
        super();
        this.serviceDate = serviceDate;
        this.vehicleRegistrationNumber = vehicleRegistrationNumber;
    }

    // Getters and Setters for every field.
    public UUID getServiceID() {
        return serviceID;
    }

    public void setServiceID(UUID serviceID) {
        this.serviceID = serviceID;
    }

    public Date getServiceDate() {
        return serviceDate;
    }

    public void setServiceDate(Date serviceDate) {
        this.serviceDate = serviceDate;
    }

    public String getVehicleRegistrationNumber() {
        return vehicleRegistrationNumber;
    }

    public void setVehicleRegistrationNumber(String vehicleRegistrationNumber) {
        this.vehicleRegistrationNumber = vehicleRegistrationNumber;
    }

    // ".equals()" method implementation to compare two "VehicleServiceRecord" objects.
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof VehicleServiceRecord that)) return false;
        return Objects.equals(getServiceID(), that.getServiceID()) && Objects.equals(getServiceDate(), that.getServiceDate()) && Objects.equals(getVehicleRegistrationNumber(), that.getVehicleRegistrationNumber());
    }

    // "hashCode()" method implementation.
    @Override
    public int hashCode() {
        return Objects.hash(getServiceID(), getServiceDate());
    }
}
