
/*
 * Represents the format in which a "response message" with the "processed information" is responded to the client.
 */

package revision.springboot.restapi.SpringBootRestApiRevision.response.generalMessage;

import revision.springboot.restapi.SpringBootRestApiRevision.entities.Vehicle;

@SuppressWarnings("unused")
public record ServerResponse(String message, Vehicle vehicle) {
}
