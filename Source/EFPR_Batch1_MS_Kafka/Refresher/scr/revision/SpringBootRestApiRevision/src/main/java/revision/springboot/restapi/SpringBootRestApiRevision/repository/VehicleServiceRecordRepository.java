
/*
 * This interface provides the implementation of "DAO(Data Access Object) / Repository" layer.
 * This performs all kinds of database operations like CREATE, UPDATE, DELETE AND SELECT etc.
 */

package revision.springboot.restapi.SpringBootRestApiRevision.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import revision.springboot.restapi.SpringBootRestApiRevision.entities.VehicleServiceRecord;

import java.util.UUID;

@Repository // This repository is responsible for CRUD operations on "VehicleServiceRecord" objects.
public interface VehicleServiceRecordRepository extends JpaRepository<VehicleServiceRecord, UUID> {
}
