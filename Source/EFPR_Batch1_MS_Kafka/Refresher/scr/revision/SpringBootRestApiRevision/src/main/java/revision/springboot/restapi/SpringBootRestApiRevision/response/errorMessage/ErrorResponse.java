
/*
 * Represents the format in which an "error" with an "error message" and "other details" is responded to the client.
 */

package revision.springboot.restapi.SpringBootRestApiRevision.response.errorMessage;

import java.util.Date;

@SuppressWarnings("unused")
public record ErrorResponse(String className, String message, Date time) {
}
