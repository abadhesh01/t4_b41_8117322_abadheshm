package revision.springboot.restapi.SpringBootRestApiRevision.service;

import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.transaction.IllegalTransactionStateException;
import revision.springboot.restapi.SpringBootRestApiRevision.entities.Owner;
import revision.springboot.restapi.SpringBootRestApiRevision.entities.Vehicle;
import revision.springboot.restapi.SpringBootRestApiRevision.error.VehicleNotFoundException;
import revision.springboot.restapi.SpringBootRestApiRevision.repository.VehicleRepository;
import revision.springboot.restapi.SpringBootRestApiRevision.repository.VehicleServiceRecordRepository;
import revision.springboot.restapi.SpringBootRestApiRevision.response.generalMessage.ServerResponse;
import revision.springboot.restapi.SpringBootRestApiRevision.response.generalMessage.ServerResponseWithDetails;

import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ServiceProviderImplTest {

    @Autowired
    @SuppressWarnings("all")
    private VehicleRepository vehicleRepository;

    @Autowired
    @SuppressWarnings("all")
    private VehicleServiceRecordRepository serviceRecordRepository;

    @Autowired
    @SuppressWarnings("all")
    private ServiceProvider serviceProvider;

    // A "Vehicle" reference to keep the track of the last dummy vehicle added to the test database.
    private Vehicle lastDummyVehicle;

    @BeforeEach
    void setUp() {
        // Creating a list of 10 dummy vehicles and adding them to the test database before testing.
        List<Vehicle> dummyVehicleList = new LinkedList<>();
        Vehicle dummyVehicle;
        for (int vehicleCount = 0; vehicleCount < 10; vehicleCount ++) {
            dummyVehicle = new Vehicle();
            dummyVehicle.setRegistrationNumber("DummyRegistration-" + vehicleCount);
            dummyVehicle.setBrand("Toyota");
            dummyVehicle.setModel("Corolla");
            dummyVehicle.setType("4 Wheeler");
            dummyVehicle.setVariant("Sedan");
            List<Owner> dummyOwners = getSampleOwnerList();
            dummyVehicle.setOwners(dummyOwners);
            Vehicle finalDummyVehicle = dummyVehicle;
            dummyOwners.forEach(dummyOwner -> dummyOwner.setVehicle(finalDummyVehicle));
            dummyVehicleList.add(dummyVehicle);
        }
        vehicleRepository.saveAll(dummyVehicleList);
    }

    // This method returns a list of sample owners.
    private static List<Owner> getSampleOwnerList() {
        List<Owner> ownerList = new LinkedList<>();
        // Owner 1
        Owner dummyOwner = new Owner();
        dummyOwner.setFirstName("Michale");
        dummyOwner.setMiddleName("Jay");
        dummyOwner.setLastName("White");
        // Owner
        Owner dummySecondOwner = new Owner();
        dummySecondOwner.setFirstName("Karl");
        dummySecondOwner.setMiddleName("Jamshed");
        dummySecondOwner.setLastName("Khandalavala");
        // Adding "Owner 1" and "Owner 2" to the sample list of owners.
        ownerList.add(dummyOwner);
        ownerList.add(dummySecondOwner);
        return ownerList;
    }

    @AfterEach
    void tearDown() {
        // Deleting all the vehicles from the database after testing.
        vehicleRepository.deleteAll();
        // Deleting all the vehicle service records from the database after testing.
        serviceRecordRepository.deleteAll();
    }

    void updateLastVehicle() {
        // This method updates the field "this.lastDummyVehicle" for everytime it is called.
        this.lastDummyVehicle = vehicleRepository.findLastVehicle();
    }

    @Test
    void getAllVehicle() {
        // Confirming that the list of vehicles extracted from the database is correct.
        assertThat(serviceProvider.getAllVehicles()).isEqualTo(vehicleRepository.findAll());
    }

    @Test
    void getVehicle() {
        updateLastVehicle(); // Updating the field "this.lastDummyVehicle".
        /* ----- "getVehicle()" ----- */
        // Extracting a vehicle from database using the serial number of "this.lastDummyVehicle".
        Vehicle extractedVehicle = (Vehicle)(serviceProvider.getVehicle(this.lastDummyVehicle.getSerialNumber()));
        // Confirming that the extracted vehicle has the same property values as of "this.lastDummyVehicle".
        assertThat(extractedVehicle.equals(this.lastDummyVehicle)).isTrue();
    }

    void addVehicle(Vehicle dummyVehicle, boolean addDummyVehicleServiceRecords) {
        /* ----- "addVehicle()" ----- */
        // Adding the dummy vehicle to the database and recording the server response.
        ServerResponseWithDetails response = serviceProvider.addVehicle(dummyVehicle, addDummyVehicleServiceRecords);

        updateLastVehicle(); // Updating the field "this.lastDummyVehicle".

        // Confirming the vehicle contained in the server response has same property values as of the added vehicle.
        assertThat(response.response().vehicle()).isEqualTo(dummyVehicle);
        // Confirming the field "this.lastDummyVehicle" has same property values as of the added vehicle.
        assertThat(this.lastDummyVehicle).isEqualTo(dummyVehicle);
        // Confirming the message contained in the server response is correct.
        assertThat(response.response().message()).isEqualTo("The vehicle has been added successfully.");
    }

    @Test
    void addVehicleWithDummyServiceRecords() {
        /*
         * This method will add new vehicle to the database with dummy vehicle service records.
         */
        // Creating a dummy vehicle.
        Vehicle dummyVehicle = new Vehicle();
        dummyVehicle.setRegistrationNumber("SecondLastVehicleInTheGarage");
        dummyVehicle.setBrand("Honda");
        dummyVehicle.setModel("Africa Twin");
        dummyVehicle.setType("2 Wheeler");
        dummyVehicle.setVariant("Off Road");
        // Calling "addVehicle" method with parameter values -
        // [1] "Vehicle dummyVehicle" : "dummyVehicle"
        // [2] "boolean addDummyVehicleServiceRecords" : "true"
        addVehicle(dummyVehicle, true);
    }

    @Test
    void addVehicleWithoutDummyServiceRecords() {
        /*
         * This method will add new vehicle to the database without dummy vehicle service records.
         */
        // Creating a dummy vehicle.
        Vehicle dummyVehicle = new Vehicle();
        dummyVehicle.setRegistrationNumber("LastVehicleInTheGarage");
        dummyVehicle.setBrand("Honda");
        dummyVehicle.setModel("Africa Twin");
        dummyVehicle.setType("2 Wheeler");
        dummyVehicle.setVariant("Off Road");
        // Calling "addVehicle" method with parameters values -
        // [1] "Vehicle dummyVehicle" : "dummyVehicle"
        // [2] "boolean addDummyVehicleServiceRecords" : "false"
        addVehicle(dummyVehicle, false);
    }

    @Test
    @SuppressWarnings("all")
    void updateVehicle() {
        // Updating the field "this.lastDummyVehicle".
        this.lastDummyVehicle = vehicleRepository.findLastVehicleWithOwners();

        // Creating a copy of the field "this.lastDummyVehicle".
        Vehicle lastDummyVehicleBeforeUpdate = new Vehicle(
                this.lastDummyVehicle.getRegistrationNumber(),
                this.lastDummyVehicle.getBrand(),
                this.lastDummyVehicle.getModel(),
                this.lastDummyVehicle.getType(),
                this.lastDummyVehicle.getVariant());
        lastDummyVehicleBeforeUpdate.setSerialNumber(this.lastDummyVehicle.getSerialNumber());
        lastDummyVehicleBeforeUpdate.setOwners(this.lastDummyVehicle.getOwners());

        // Creating another copy of the field "this.lastDummyVehicle" with updated fields
        // along with serial number for posting the desired updates.
        Vehicle lastDummyVehicleCopy = new Vehicle();
        lastDummyVehicleCopy.setSerialNumber(this.lastDummyVehicle.getSerialNumber());
        lastDummyVehicleCopy.setRegistrationNumber("TestBikeUpdate");
        lastDummyVehicleCopy.setBrand("Hero MotoCorp");
        lastDummyVehicleCopy.setModel("XPulse 200T");
        lastDummyVehicleCopy.setType("2 Wheeler");
        lastDummyVehicleCopy.setVariant("Off Road");
        List<Owner> lastDummyVehicleCopyOwners = new LinkedList<>();
        this.lastDummyVehicle.getOwners().forEach(ownner -> {
            Owner existingOwner = new Owner();
            existingOwner.setId(ownner.getId());
            existingOwner.setFirstName(ownner.getFirstName() + "[UPDATED]");
            existingOwner.setMiddleName(ownner.getMiddleName() + "[UPDATED]");
            existingOwner.setLastName(ownner.getLastName() + "[UPDATED]");
            lastDummyVehicleCopyOwners.add(existingOwner);
        });
        lastDummyVehicleCopyOwners.addAll(getSampleOwnerList());
        lastDummyVehicleCopy.setOwners(lastDummyVehicleCopyOwners);

        /* ----- "updateVehicle()" ----- */
        // Saving / Updating the property values of copy of "this.lastDummyVehicle" to the database.
        ServerResponse response = serviceProvider.updateVehicle(lastDummyVehicleCopy);

        updateLastVehicle(); // Updating the field "this.lastDummyVehicle".

        // Confirming the vehicle contained in the server response has same property values as of "this.lastDummyVehicle".
        assertThat(response.vehicle().equals(this.lastDummyVehicle)).isTrue();
        // Confirming the vehicle extracted from the database using the serial number of "this.lastDummyVehicle"
        // has same property values as of the "this.lastDummyVehicle".
        assertThat(vehicleRepository.findById(this.lastDummyVehicle.getSerialNumber()).get().equals(this.lastDummyVehicle)).isTrue();

        // Confirming the vehicle contained in the server response does not have the same property values as of
        // the copy of "this.lastDummyVehicle"( crerated before updation of "this.lastDummyVehicle").
        assertThat(response.vehicle().equals(lastDummyVehicleBeforeUpdate)).isFalse();
        // Confirming the vehicle extracted from the database using the serial number of "this.lastDummyVehicle"
        // does not have the same property values as of the copy of "this.lastDummyVehicle"( crerated before updation of "this.lastDummyVehicle").
        assertThat(vehicleRepository.findById(this.lastDummyVehicle.getSerialNumber()).get().equals(lastDummyVehicleBeforeUpdate)).isFalse();

        // Confirming the message contained in the server response is correct.
        assertThat(response.message().equals("The vehicle has been updated successfully.")).isTrue();
    }

    @Test
    void deleteVehicle() {
        updateLastVehicle(); // Updating the field "this.lastDummyVehicle".

        /* ----- "deleteVehicle()" ----- */
        // Deleting a vehicle from the database using the serial number of "this.lastDummyVehicle" and recording the server response.
        ServerResponse response = serviceProvider.deleteVehicle(this.lastDummyVehicle.getSerialNumber());

        // Confirming the vehicle has been deleted from the database by extracting it from the database using
        // the serial number of "this.lastDummyVehicle" and confirming that the extracted value is EMPTY.
        assertThat(vehicleRepository.findById(this.lastDummyVehicle.getSerialNumber()).isEmpty()).isTrue();

        // Confirming the message contained in the server response is correct.
        assertThat(response.message()
                .equals(
                        "The vehicle with serial number '" + this.lastDummyVehicle.getSerialNumber()
                                + "' has been deleted successfully."))
                .isTrue();

        // Confirming that the vehicle contained in the response is NULL.
        assertThat(response.vehicle()).isNull();
    }

    @Test
        // Test for extracting, updating, and deleting a vehicle using a non-existing serial number.
    void checkForSerialNumberNotFound() {
        updateLastVehicle(); // Updating the field "this.lastDummyVehicle".

        // Creating a non-existing serial number by adding "+1" to the serial number of "this.lastDummyVehicle".
        long nonExistingSerialNumber = this.lastDummyVehicle.getSerialNumber() + 1;

        /* ----- "getVehicle()" ----- */
        // Confirming that extracting a vehicle from the database using a non-existing serial number
        // throws "VehicleNotFoundException" with appropriate message.
        assertThatThrownBy(() ->
                serviceProvider.getVehicle(nonExistingSerialNumber)).isInstanceOf(VehicleNotFoundException.class)
                .hasMessageContaining("Vehicle with serial number '"
                        + nonExistingSerialNumber
                        + "' was not found!");

        /* ----- "updateVehicle()" ----- */
        // Confirming that updating a vehicle from the database using a non-existing serial number
        // throws "VehicleNotFoundException" with appropriate message.
        assertThatThrownBy(() -> {
            Vehicle dummyVehicle = new Vehicle();
            dummyVehicle.setSerialNumber(nonExistingSerialNumber);
            dummyVehicle.setRegistrationNumber("");
            serviceProvider.updateVehicle(dummyVehicle); // Updating here.
        }).isInstanceOf(VehicleNotFoundException.class)
                .hasMessageContaining("Vehicle with serial number '"
                        + nonExistingSerialNumber
                        + "' was not found!");

        /* ----- "deleteVehicle()" ----- */
        // Confirming that deleting a vehicle from the database using a non-existing serial number
        // throws "VehicleNotFoundException" with appropriate message.
        assertThatThrownBy(() -> serviceProvider.deleteVehicle(nonExistingSerialNumber))
                .isInstanceOf(VehicleNotFoundException.class)
                .hasMessageContaining("Vehicle with serial number '"
                        + nonExistingSerialNumber
                        + "' was not found!");
    }

    @Test @SuppressWarnings("all")
        // Test for adding a new vehicle to the database with existing registration number
        // and another new vehicle to the database with NULL registration number.
    void checkForAddingExistingAndNullRegistrationNumber() {
        updateLastVehicle(); // Updating the field "this.lastDummyVehicle".

        // Confirming that adding a vehicle to the database with an existing registration number will
        // throw "DataIntegrityViolationException".
        // In the below test, we are adding a new vehicle to the database which has the same registration number
        // of "this.lastDummyVehicle".
        Vehicle vehicleWithExistingRegistrationNumber = new Vehicle(
                this.lastDummyVehicle.getRegistrationNumber(), "", "", "", ""
        );
        vehicleWithExistingRegistrationNumber.setOwners(new LinkedList<>());
        assertThatThrownBy(() ->
                serviceProvider.addVehicle(vehicleWithExistingRegistrationNumber, false)
        ).isInstanceOf(DataIntegrityViolationException.class);

        // Confirming that adding a vehicle to the database with NULL registration number will
        // throw "DataIntegrityViolationException".
        Vehicle vehicleWithNullRegistrationNumber = new Vehicle(
                null, "", "", "", ""
        );
        vehicleWithNullRegistrationNumber.setOwners(new LinkedList<>());
        assertThatThrownBy(() ->
                serviceProvider.addVehicle(vehicleWithNullRegistrationNumber,
                        false))
                .isInstanceOf(DataIntegrityViolationException.class);
    }

    @Test
    void getDummyServiceRecords() {
        // Checking for calling the "getDummyServiceRecords()" method directly will always generate
        // "org.springframework.transaction.IllegalTransactionStateException" with message
        // "No existing transaction found for transaction marked with propagation 'mandatory'",
        // whenever it is called directly.

        // [Test Case 1]: enableAddDummyVehiclesToServiceRecords = true
        assertThatThrownBy(() -> serviceProvider.getDummyServiceRecords(
                "SampleVehicleRegistrationNumber",
                true // enableAddDummyVehiclesToServiceRecords
        )).isInstanceOf(IllegalTransactionStateException.class)
                .hasMessageContaining(
                        "No existing transaction found for transaction marked with propagation 'mandatory'"
                );

        // [Test Case 2]: enableAddDummyVehiclesToServiceRecords = false
        assertThatThrownBy(() -> serviceProvider.getDummyServiceRecords(
                "SampleVehicleRegistrationNumber",
                false // enableAddDummyVehiclesToServiceRecords
        )).isInstanceOf(IllegalTransactionStateException.class)
                .hasMessageContaining(
                        "No existing transaction found for transaction marked with propagation 'mandatory'"
                );
    }
}
